/**
* Program : dice_roller
* Author : Phillip Dudley ( Predatorian3@gmail.com )
* Version : v0.1
* Description : dice_roller is just that, a dice roller. 
*/

/** The includes */
#include <stdio.h>

// The Custom Headers
#include "logging.h"
#include "roller.h"

int main(int argc, char *argv[])
{
	logger( "Program Started", 1 );
	printf( "Welcome to the Dice Roller\n" );
	int rolls;
	int dieSides;
	for( ; ; )
	{
		printf( "Please enter the number of rolls, or enter the number 0 to exit the program\n" );
		scanf( "%d", &rolls ); // only works with integers. otherwise, do not reference the variable
		if( rolls == 0 )
		{
			break;
		}
		printf( "Please enter the number of faces on the die. \n" );
		scanf( "%d", &dieSides );
		roller( rolls, dieSides ); 
	} 
	logger( "Program terminated", 1 );
	
	return 0;
}
